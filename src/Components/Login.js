import React from "react";
import agent from '../agent';
import { connect } from 'react-redux';
import {
  UPDATE_FIELD_AUTH,
  LOGIN,
} from '../Actions/constants';

//TODO
//здесь нужно просто подключить токен к стору и убрать из app
const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({
  onChangeEmail: value =>
    dispatch({ type: UPDATE_FIELD_AUTH, key: 'email', value }),
  onChangePassword: value =>
    dispatch({ type: UPDATE_FIELD_AUTH, key: 'password', value }),
  onSubmit: (email, password) =>
    dispatch({ type: LOGIN, payload: agent.Auth.login(email, password) }),
});

class Login extends React.Component {
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    this.submitForm = (email, password) => ev => {
      console.log("submitFormLogin", email, password);
      ev.preventDefault();
      this.props.onSubmit(email, password);
    };
  }

  componentWillUnmount() {
  }

  render() {
    const email = this.props.login.email ? this.props.login.email : '';
    const password = this.props.login.password ? this.props.login.password : '';
    return (
      <div style={{paddingTop:'20px'}}>
        <h1>Sign In</h1>

        <form style={{paddingTop:'10px'}} onSubmit={this.submitForm(email, password)}>
          <fieldset>

            <fieldset>
              <input
                type="text"
                placeholder="Email"
                value={email}
                onChange={this.changeEmail} />
            </fieldset>

            <fieldset>
              <input
                type="password"
                placeholder="Password"
                value={password}
                onChange={this.changePassword} />
            </fieldset>

            <button
              type="submit">
              Sign in
                  </button>

          </fieldset>
        </form>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);


