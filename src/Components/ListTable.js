import React from "react";
import ReactTable from "react-table";
import { connect } from 'react-redux';
import "react-table/react-table.css";
import _ from "lodash";
import agent from '../agent';
import { LOAD_COMMENTS, EDIT_COMMENT } from "../Actions/constants";

const ProcessData = (comments, pageSize, page, sorted, filtered, total_task_count) => {
  
  let filteredData = comments;

  if (filtered.length) {
    filteredData = filtered.reduce((filteredSoFar, nextFilter) => {
      return filteredSoFar.filter(row => {
        return (row[nextFilter.id] + "").includes(nextFilter.value);
      });
    }, filteredData);
  }
  
  const sortedData = _.orderBy(
    filteredData,
    sorted.map(sort => {
      return row => {
        if (row[sort.id] === null || row[sort.id] === undefined) {
          return -Infinity;
        }
        return typeof row[sort.id] === "string"
          ? row[sort.id].toLowerCase()
          : row[sort.id];
      };
    }),
    sorted.map(d => (d.desc ? "desc" : "asc"))
  );

  return {
    rows: sortedData,
    pages: Math.ceil(total_task_count / pageSize)
  };
};

const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({
  LoadComments: comments =>
    dispatch({ type: LOAD_COMMENTS, payload: comments }),
  UpdateComment: value =>
    dispatch({ type: EDIT_COMMENT, paylaod: value }),
});

class ListTable extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      pages: null,
      loading: true
    };
    this.onLoadComments = (values) => this.props.LoadComments(values);
    this.fetchData = this.fetchData.bind(this);
    this.renderEditable = this.renderEditable.bind(this);
    this.renderEditableStatus = this.renderEditableStatus.bind(this);
    this.onUpdateComment = (values) => {
      console.log("we aree here onUpdateComment")
      Promise.resolve(agent.Comments.edit(values))
        .then((response) => {
          if (_.has(response, 'status') && response.status == "ok") {
            return this.props.UpdateComment({
              values
            })
          } else alert(`Unsucceeded edit with response ${JSON.stringify(response)}`);
        }) 
    };
  }

  //LoadComments
  fetchData(state) {

    this.setState({ loading: true });
    let sort_field, sort_direction;

    if (state && state.sorted.length > 0) {
      sort_field = state.sorted[0].id;
      sort_direction = state.sorted[0].desc ? "desc" : "asc";
    }

    return Promise.resolve(agent.Comments.all(state.page, sort_field, sort_direction))
      .then((response) => {
        console.log("respose Get", response)
        if (response && response.status && response.status === "ok") {
          let comments = _.get(response, "message.tasks");
          let total_task_count = _.get(response, "message.total_task_count");

          this.onLoadComments(comments)
          const sortedResponse = ProcessData(
            comments,
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
            total_task_count
            )

          this.setState({
            data: sortedResponse.rows,
            pages: sortedResponse.pages,
            loading: false
          });
        } else {
          this.setState({
            data: [],
            pages: null,
            loading: false
          });
        }
      }).catch((err) => console.log("err", err));
  }

  validate(columnId, status) {
    console.log("columnID", status );
    if( columnId === "status" && (status === "0" || status === "10")) {
      return true;
    } else if (columnId === "text") return true;
    else return false;
  }

  renderEditable(cellInfo) {
    console.log("renderEditable", cellInfo)
    const data = [...this.state.data];
    return (
      <div
        style={{ backgroundColor: "#fafafa" }}
        contentEditable
        suppressContentEditableWarning
        onBlur = { e => {
        
            data[cellInfo.index][cellInfo.column.id] = e.target.innerHTML;
            this.setState({ data });
            alert("You've changed the field. Click Update to save."); 
        
          
        }}
        dangerouslySetInnerHTML= {{__html:  this.state.data[cellInfo.index][cellInfo.column.id]}}
        />
    );
  }

  renderEditableStatus(cellInfo) {
    console.log("renderEditableStatus", cellInfo)
    const data = [...this.state.data];
    return (
      <div
        // style={{ backgroundColor: "#fafafa" }}
        contentEditable
        suppressContentEditableWarning
     
        onClick = { e => {
          data[cellInfo.index][cellInfo.column.id] = (data[cellInfo.index][cellInfo.column.id] == "0") ? "10" : "0";
          this.setState({ data });
          alert("You've changed the field. Click Update."); 

        }}
        dangerouslySetInnerHTML={this.createCheck(this.state.data[cellInfo.index][cellInfo.column.id])}
        />
    );
  }

  createCheck(value) {
    let checked = (value == 0) ? '' : "checked";
    return {__html: `<input type="checkbox" ${checked}>`};
  }

  render() {
    const { data, pages, loading } = this.state;
    const token = _.has(this.props, 'login.token') ? this.props.login.token : null;
    return (
      <div style={{paddingTop:'20px'}}>
        <h1>Comments</h1>
        <ReactTable
          columns={[
            {
              Header: "ID",
              accessor: "id",
              id: "id",
              maxWidth: 150
            },
            {
              Header: "Username",
              accessor: "username",
              id: "username",
              maxWidth: 200
            },
            {
              Header: "Email",
              accessor: "email",
              id: "email",
              maxWidth: 200
            },
            {
              Header: "Text",
              accessor: "text",
              id: "text",
              Cell: token ? this.renderEditable : (row) => (
                <span style={{ whiteSpace: 'pre-wrap', wordWrap: 'break-word' }} >
                  {row.value}
                </span>
              )
            },
            {
              Header: "Status",
              accessor: "status",
              Cell: token ? this.renderEditableStatus : (row) => (
                <span>
                  {(row.value === 0) ? 
                  (<p> not done </p> ) :
                  (<p> done </p>  )
                }
                </span>
              ),
              maxWidth: 100
            },
            {
              Header: "Update",
              id: "update",
              show: token ? true : false,
              Cell: (row) => (
                <button onClick={() => this.onUpdateComment(row.original)}>
                  Update
                </button>
              ),
              maxWidth: 100
            }
          ]}
          manual // Forces table not to paginate or sort automatically, so we can handle it server-side
          data={data}
          pages={pages} // Display the total number of pages
          loading={loading} // Display the loading overlay when we need it
          onFetchData={this.fetchData} // Request new data when things change
          //   filterable
          defaultPageSize={3}
          className="-striped -highlight"
        />
        <br />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListTable);