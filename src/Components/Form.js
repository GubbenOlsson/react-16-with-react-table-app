import React from 'react'
import { Field, reduxForm } from 'redux-form'

const required = value => (value || typeof value === 'number' ? undefined : 'Required');
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
  'Invalid email address' : undefined;

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
)
const renderFieldText = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <textarea {...input} placeholder={label} type={type} />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
)

const NewComment = props => {
  const { handleSubmit, pristine, reset, submitting } = props
  return (
    <div style={{paddingTop:'10px'}}>
      <h1>Add new comment</h1>
      <form style={{paddingTop:'10px', fontSize: '14px'}} onSubmit={handleSubmit}>
        <div>
          <label>Username</label>
          <div>
            <Field
              name="username"
              component={renderField}
              type="text"
              placeholder="username"
              validate={[required]}
            />
          </div>
        </div>
        <div>
          <label>Email</label>
          <div>
            <Field
              name="email"
              component={renderField}
              type="email"
              placeholder="email"
              validate={[required, email]}
            />
          </div>
        </div>
        <div>
          <label>Comment Text</label>
          <div>
            <Field name="text" component={renderFieldText} validate={[required]} />
          </div>
        </div>
        <div>
          <button type="submit" disabled={pristine || submitting}>
            Submit
        </button>
          <button type="button" disabled={pristine || submitting} onClick={reset}>
            Clear Values
        </button>
        </div>
      </form>
    </div>
  )
}

export default reduxForm({
  form: 'newCommentForm' // a unique identifier for this form
})(NewComment)
