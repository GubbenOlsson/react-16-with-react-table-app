import 'jsdom-global/register'; 

import React from 'react';

import {Provider} from "react-redux"
import { shallow, mount } from 'enzyme';

import configureStore from 'redux-mock-store'

import App from '../App';

describe('It tests main functions of Component <App />', () => {

  const initialState = {
    visible: false
  };

  const mockStore = configureStore();
  let store;

  store = mockStore(initialState)
  
  it('renders without crashing', () => {

    const wrapper = mount(<Provider store={store}>
      <App />
    </Provider>);
  });

});
