
export const create = 
[
  {
    "id": 1371,
    "username": "Example",
    "email": "example@example.com",
    "text": "QWEQWQWEQWE",
    "status": 10,
    "image_path": "https://uxcandy.com/~shapoval/test-task-backend/upload/user_images/1107fb5b/1524207101_avatarDoter.jpg"
  }
];

export const get = 
{
  "status": "ok",
  "message": {
      "tasks": [
          {
              "id": 5073,
              "username": "Test User",
              "email": "test_user_1@example.com",
              "text": "Hello, world!",
              "status": 10,
              "image_path": "https://uxcandy.com/~shapoval/test-task-backend/upload/user_images/1107fb5b/1524207101_avatarDoter.jpg"
          },
          {
              "id": 5074,
              "username": "Test User 2",
              "email": "test_user_2@example.com",
              "text": "Hello from user 2!",
              "status": 0,
              "image_path": "https://uxcandy.com/~shapoval/test-task-backend/upload/user_images/1107fb5b/1524207101_avatarDoter.jpg"
          },
          {
              "id": 5075,
              "username": "Test User 3",
              "email": "test_user_3@example.com",
              "text": "Hello from user 3!",
              "status": 0,
              "image_path": "https://uxcandy.com/~shapoval/test-task-backend/upload/user_images/1107fb5b/1524207101_avatarDoter.jpg"
          }
      ],
      "total_task_count": "5"
  }
};