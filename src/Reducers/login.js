import { UPDATE_FIELD_AUTH, LOGIN } from '../Actions/constants';


export default (state = {}, action) => {
   switch (action.type) {

       case LOGIN:

           return {
               ...state,
               currentUser: action.payload ? action.payload.user : null,
               token:  action.payload ? action.payload.token : null,
               errors: action.error ? action.payload.errors : null
           };

       case UPDATE_FIELD_AUTH:
           return { ...state, [action.key]: action.value };

       default:
           return state;
   }
};